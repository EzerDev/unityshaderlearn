﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/003-phong"
{
   // Phong 模型高光
   Properties
   {
       _Diffuse("Diffuse", Color) = (1,1,1,1)
       _Specular("Specular", Color) = (1,1,1,1)
       _Gloss("Gloss", float) = 1.0
   }

   SubShader
   {
       Tags{"LightMode"="ForwardBase"}
       Pass
       {
           CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"
                #include "Lighting.cginc"

                fixed4 _Diffuse;
                fixed4 _Specular;
                float _Gloss;

                struct a2v
                {
                    float4 vertex : POSITION;
                    float3 normal : NORMAL;
                };

                struct v2f
                {
                    float4 pos : SV_POSITION;
                    float4 worldPos : TEXCOORD0;
                    float3 worldNormal : TEXCOORD1;
                };

                v2f vert(a2v v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                    o.worldNormal = mul(v.normal, (float3x3)unity_ObjectToWorld);
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    // 环境光
                    fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
                    // 漫反射
                    float3 worldNormal = normalize(i.worldNormal);
                    float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
                    float halfLambert = dot(worldNormal, lightDir) * 0.5 + 0.5;
                    fixed3 diffuse = _LightColor0.rgb * _Diffuse.rgb * halfLambert;
                    // 高光反射
                    float3 reflectDir = normalize(reflect(-lightDir , worldNormal));
                    float3 viewDir = normalize( _WorldSpaceCameraPos.xyz - i.worldPos.xyz );
                    fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(saturate(dot(viewDir, reflectDir)), _Gloss);

                    fixed4 color = fixed4(ambient + diffuse + specular, 1.0);
                    return color;
                }

           ENDCG
       }
   }
}
