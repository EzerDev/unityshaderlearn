﻿Shader "Unlit/009-alphablend-test"
{
     Properties
    {
        _MainTex("MainTex", 2D) = "white"{}
        _AlphaScale("AlphaScale", Range(0, 1)) = 1
        _Specular("Specular", Color) = (1,1,1,1)
        _Gloss("Gloss", Range(1, 256)) = 8.0
    }

    SubShader
    {
        Tags{"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}

        Pass
        {
            ZWrite On
            ColorMask 0
        }

        Pass
        {
            Tags{"LightMode"="ForwardBase"}
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _AlphaScale;
            fixed4 _Specular;
            float _Gloss;

            struct a2v
            {
                float4 vertex : POSITION;
                float4 normal : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 viewDir : TEXCOORD1;
                float3 lightDir : TEXCOORD2;
                float3 worldNormal : TEXCOORD3;
            };

            v2f vert(a2v i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);
                float3 worldPos = mul(unity_ObjectToWorld, i.vertex);
                o.lightDir = UnityWorldSpaceLightDir(worldPos);
                o.viewDir = UnityWorldSpaceViewDir(worldPos);
                o.worldNormal = UnityObjectToWorldNormal(i.normal);

                o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 lightDir = normalize(i.lightDir);
                float3 viewDir = normalize(i.viewDir);
                float3 normal = normalize(i.worldNormal);

                fixed4 texColor = tex2D(_MainTex, i.uv);

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
                // diffuse
                float halfLambert = dot(normal, lightDir) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * texColor.rgb * halfLambert;
                // specular
                float3 halfDir = normalize(viewDir + lightDir);
                fixed3 specular = _LightColor0.rgb * _Specular * pow(saturate(dot(halfDir, normal)), _Gloss);

                return fixed4(ambient + diffuse + specular, texColor.a * _AlphaScale); 
            }

            ENDCG
        }
    }
}
