﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/001"
{
    Properties
    {
        // 声明一个颜色Properties
        _Color ("Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            fixed4 _Color;

            // 使用一个结构体来定义 顶点着色器的输入
            struct a2v
            {
                // POSITION 语意： 告诉Unity用模型空间顶点坐标填充 vertex变量
                float4 vertex : POSITION;
                // NORMAL 用模型空间的法线方向 填充
                float3 normal : NORMAL;
                // TEXCOORD0 用模型的第一套纹理坐标填充texcoord 变量
                float4 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                // SV_POSITION ： pos中包含了顶点在裁剪空间中的坐标
                float4 pos : SV_POSITION;
                fixed3 color : COLOR0;
            };
            
            // POSITION的语意为 模型顶点的位置
            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                // v.normal 包含了顶点的法线方向， 其分量范围在[-1.0 , 1.0]
                o.color = v.normal * 0.5 + fixed3(0.5, 0.5, 0.5);
                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET
            {
                fixed3 color = _Color.rgb * i.color;
                return fixed4(color, 1);
            }

            ENDCG
        }
    }
}
