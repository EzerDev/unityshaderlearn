﻿Shader "Unlit/001-test"
{
    SubShader
    {
        pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            struct v2f
            {
                float4 pos : SV_POSITION;
                fixed4 color : COLOR0;
            };

            v2f vert(appdata_full v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                // 可视化法线方向
                o.color = fixed4(v.normal * 0.5 + fixed3(0.5, 0.5, 0.5), 1.0);

                // 可视化切线方向
                o.color = fixed4(v.tangent * 0.5 + fixed3(0.5, 0.5, 0.5), 1.0);

                // 可视化副切线方向 求出tangent 与法线 组成坐标系的副切线， v.tangent.w为 坐标系方向参数
                float3 bitangent = cross(v.tangent.xyz, v.normal) * v.tangent.w;
                o.color = fixed4(bitangent * 0.5 + fixed3(0.5, 0.5, 0.5), 1.0);

                // 可视化第一组纹理坐标
                o.color = fixed4(v.texcoord.xy , 0.0, 1.0);

                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET
            {
                return i.color;
            }

            ENDCG
        }
    }
}
