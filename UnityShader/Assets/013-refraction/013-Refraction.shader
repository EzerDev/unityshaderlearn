﻿Shader "Unlit/013-Refraction"
{
    Properties
  {
      _Color ("Color", Color) = (1,1,1,1)
      _RefractColor ("Refraction Color", Color) = (1,1,1,1)
      _RefractAmount ("Refraction Amount", Range(0, 1)) = 1
      _RefractRatio ("Refraction Ratio", Range(0.1, 1)) = 0.5
      _CubeMap ("Refraction Cubemap", Cube) = "_Skybox" {}
  }
  
  SubShader
  {
      Tags{"LightMode"="ForwardBase"}
      Pass
      {
          CGPROGRAM

          #pragma vertex vert
          #pragma fragment frag
          #include "UnityCG.cginc"
          #include "Lighting.cginc"
          #include "AutoLight.cginc"

          fixed4 _Color;
          fixed4 _RefractColor;
          float _RefractAmount;
          float _RefractRatio;
          samplerCUBE _CubeMap;

          struct a2v
          {
              float4 vertex : POSITION;
              float3 normal : NORMAL;
          };

          struct v2f
          {
              float4 pos : SV_POSITION;
              float3 worldPos : TEXCOORD0;
              float3 worldViewDir : TEXCOORD1;
              float3 worldNormal : TEXCOORD2;
              float3 worldRefr : TEXCOORD3;
              SHADOW_COORDS(2)
          };

          v2f vert(a2v i)
          {
              v2f o;
              o.pos = UnityObjectToClipPos(i.vertex);
              o.worldPos = mul(unity_ObjectToWorld, i.vertex);
              o.worldViewDir = UnityWorldSpaceViewDir(o.worldPos);
              o.worldNormal = UnityObjectToWorldNormal(i.normal);
              o.worldRefr = refract(-normalize(o.worldViewDir), normalize(o.worldNormal), _RefractRatio);
              TRANSFER_SHADOW(o);
              return o;
          }

          fixed4 frag(v2f i) : SV_TARGET
          {
              fixed3 worldViewDir = normalize(i.worldViewDir);
              fixed3 worldNormal = normalize(i.worldNormal);
              fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

              fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;

              float halfLambert = dot(worldLightDir, worldNormal) * 0.5 + 0.5;
              fixed3 diffuse = _LightColor0.rgb * _Color.rgb * halfLambert;

              fixed3 refraction = texCUBE(_CubeMap, i.worldRefr).rgb * _RefractColor.rgb;

              UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);

              fixed3 color = ambient + lerp(diffuse, refraction, _RefractAmount) * atten;
              
              return fixed4(color, 1);
          }


          ENDCG
      }
  }

}
