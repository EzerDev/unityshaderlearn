﻿Shader "Unlit/006-ramp"
{
    Properties
    {
        _RampTex("RampTex", 2D) = "white"{}
        _Color("Color", Color) = (1,1,1,1)

        _Specular("Specular", Color) = (1,1,1,1)
        _Gloss("Gloss", Range(8, 256)) = 8
    }

    SubShader
    {
        Tags{"LightMode"="ForwardBase"}
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _RampTex;
            fixed4 _Specular;
            float _Gloss;
            fixed4 _Color;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 worldPos : TEXCOORD0;
                float3 worldNormal : TEXCOORD1;
            };

            v2f vert(a2v i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);
                o.worldPos = mul(unity_ObjectToWorld, i.vertex);
                o.worldNormal = UnityObjectToWorldNormal(i.normal);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                float3 lightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
                // 渐变纹理 就直接 使用纹理作为漫反射颜色
                // 漫反射
                float halfLambert = dot(i.worldNormal, lightDir) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * tex2D(_RampTex, float2(halfLambert, halfLambert)).rgb * _Color.rgb;

                // 高光反射
                float halfDir = normalize(viewDir + lightDir);
                fixed3 specular = _LightColor0.rgb * _Specular * pow(saturate(dot(i.worldNormal, halfDir)), _Gloss);

                fixed3 color = ambient + diffuse + specular;

                return fixed4(color, 1.0);
            }

            ENDCG
        }
    }
}
