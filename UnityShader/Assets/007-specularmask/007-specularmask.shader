﻿Shader "Unlit/007-specularmask"
{
    Properties
    {
        _MainTex("MainTex", 2D) = "white"{}
        _Color("Color", Color) = (1,1,1,1)

        _BumpTex("BumpTex", 2D) = "white"{}
        _BumpScale("BumpScale", float) = 1.0

        _Specular("Specular", Color) = (1,1,1,1)
        _SpecularMask("SpecularMask", 2D) = "white" {}
        _SpecularScale("SpecularScale", float) = 1.0
        _Gloss("Gloss", Range(8.0, 256)) = 8.0
    }

    SubShader
    {
        Tags{"LightMode"="ForwardBase"}
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;

            sampler2D _BumpTex;
            float _BumpScale;

            fixed4 _Specular;
            sampler2D _SpecularMask;
            float _SpecularScale;
            float _Gloss;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 viewDir : TEXCOORD1;
                float3 lightDir : TEXCOORD2;
            };

            v2f vert(a2v i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);
                float3 binormal = cross(i.normal,i.tangent.xyz) * i.tangent.w;
                float3x3 rotation = float3x3(i.tangent.xyz, binormal, i.normal);
                o.viewDir = mul(rotation, ObjSpaceViewDir(i.vertex));
                o.lightDir = mul(rotation, ObjSpaceLightDir(i.vertex));
                o.uv = TRANSFORM_TEX(i.texcoord, _MainTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 viewDir = normalize(i.viewDir);
                float3 lightDir = normalize(i.lightDir);

                fixed3 bump = UnpackNormal(tex2D(_BumpTex, i.uv));
                bump.xy *= _BumpScale;

                fixed3 albedo = tex2D(_MainTex, i.uv) * _Color;

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;

                // 漫反射
                float halfLambert = dot(lightDir, bump) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * albedo * halfLambert;

                // 高光
                fixed specularMask = tex2D(_SpecularMask, i.uv).r * _SpecularScale;
                fixed3 halfDir = normalize(lightDir + viewDir);
                fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(saturate(dot(bump, halfDir)), _Gloss) * specularMask;

                fixed3 color = ambient + diffuse + specular;
                return fixed4(color, 1.0);
            }

            ENDCG
        }
    }
}
