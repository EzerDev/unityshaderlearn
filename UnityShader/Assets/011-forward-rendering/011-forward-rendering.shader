﻿// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'

// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'

Shader "Unlit/011-forward-rendering"
{
   Properties
   {
       _Diffuse("Diffuse", Color) = (1,1,1,1)
       _Specular("Specular", Color) = (1,1,1,1)
       _Gloss("Gloss", Range(8, 256)) = 8.0
   }

   SubShader
   {
       Pass
       {
           Tags{"LightMode"="ForwardBase"}

           CGPROGRAM
           #pragma multi_compile_fwdbase

           #pragma vertex vert
           #pragma fragment frag
           #include "UnityCG.cginc"
           #include "Lighting.cginc"
           #include "AutoLight.cginc"

           

           fixed4 _Diffuse;
           fixed4 _Specular;
           float _Gloss;

           struct a2v
           {
               float4 vertex : POSITION;
               float3 normal : NORMAL;
           };

           struct v2f
           {
               float4 pos : SV_POSITION;
               float3 worldPos : TEXCOORD0;
               float3 worldNormal : TEXCOORD1;
               SHADOW_COORDS(2)

           };

           v2f vert(a2v i)
           {
               v2f o;
               o.pos = UnityObjectToClipPos(i.vertex);
               o.worldPos = mul(unity_ObjectToWorld, i.vertex);
               o.worldNormal = UnityObjectToWorldNormal(i.normal);

               TRANSFER_SHADOW(o)
               return o;
           }

           fixed4 frag(v2f i) : SV_Target
           {
               float3 normal = normalize(i.worldNormal);
               float3 lightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
               float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
               // ambient
               fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
               // diffuse
               float halfLambert = dot(normal, lightDir) * 0.5 + 0.5;
               fixed3 diffuse = _LightColor0.rgb * _Diffuse * halfLambert;
               // specular
               float3 halfDir = normalize(viewDir + lightDir);
               fixed3 specular = _LightColor0.rgb * _Specular * pow(saturate(dot(normal, halfDir)), _Gloss);
               // 只计算阴影
               // fixed shadow = SHADOW_ATTENUATION(i)
               // fixed atten = 1.0;
               // UNITY_LIGHT_ATTENUATION not only compute attenuation, but also shadow infos
               UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
               fixed3 color = ambient + diffuse * atten + specular;
               return fixed4(color, 1.0);
           }

           ENDCG
       }

       Pass
       {
           Tags{"LightMode"="ForwardAdd"}
           
           Blend One One

           CGPROGRAM
           
           #pragma multi_compile_fwdadd
           #pragma vertex vert
           #pragma fragment frag
           #include "UnityCG.cginc"
           #include "Lighting.cginc"
           #include "UnityDeferredLibrary.cginc"

           fixed4 _Diffuse;
           fixed4 _Specular;
           float _Gloss;

           struct a2v
           {
               float4 vertex : POSITION;
               float3 normal : NORMAL;
           };

           struct v2f
           {
               float4 pos : SV_POSITION;
               float3 worldPos : TEXCOORD0;
               float3 worldNormal : TEXCOORD1;
           };

           v2f vert(a2v i)
           {
               v2f o;
               o.pos = UnityObjectToClipPos(i.vertex);
               o.worldPos = mul(unity_ObjectToWorld, i.vertex);
               o.worldNormal = UnityObjectToWorldNormal(i.normal);
               return o;
           }

           fixed4 frag(v2f i) : SV_Target
           {
               float3 normal = normalize(i.worldNormal);
               float3 viewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
                #ifdef USING_DIRECTIONAL_LIGHT
                    fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz);
                    fixed atten = 1.0;
                #else
                    fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz - i.worldPos.xyz);
                    float3 lightCoord = mul(unity_WorldToLight, float4(i.worldPos, 1)).xyz;
                    fixed atten = tex2D(_LightTexture0, dot(lightCoord, lightCoord).rr).UNITY_ATTEN_CHANNEL;
                #endif

               // ambient
               fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;
               // diffuse
               float halfLambert = dot(normal, worldLightDir) * 0.5 + 0.5;
               fixed3 diffuse = _LightColor0.rgb * _Diffuse * halfLambert;
               // specular
               float3 halfDir = normalize(viewDir + worldLightDir);
               fixed3 specular = _LightColor0.rgb * _Specular * pow(saturate(dot(normal, halfDir)), _Gloss);

               fixed3 color = ambient + diffuse + specular;
               return fixed4(color * atten, 1.0);
           }
           ENDCG
       }
   }
   Fallback "Diffuse"
}
