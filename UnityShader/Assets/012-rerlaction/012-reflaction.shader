﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/012-reflaction"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _ReflectColor ("Reflection Color", Color) = (1,1,1,1)
        _ReflectAmount ("Reflect Amount", Range(0, 1)) = 1
        _CubeMap ("Reflection Cubemap", Cube) = "_Skybox" {}
    }

    SubShader
    {
        Pass
        {
            Tags{"LightMode"="ForwardBase"}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            fixed4 _Color;
            fixed4 _ReflectColor;
            float _ReflectAmount;
            samplerCUBE _CubeMap;

            struct a2v
            {
                float4 vertex: POSITION;
                float3 normal: NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldPos: TEXCOORD0;
                float3 worldViewDir: TEXCOORD1;
                float3 worldNormal: TEXCOORD2;
                float3 worldRefl: TEXCOORD3;
                SHADOW_COORDS(2)
            };

            v2f vert(a2v i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);

                o.worldPos = mul(unity_ObjectToWorld, i.vertex);

                o.worldNormal = UnityObjectToWorldNormal(i.normal);

                o.worldViewDir = UnityWorldSpaceViewDir(o.worldPos);
                // 计算改点处的反射方向
                o.worldRefl = reflect(-o.worldViewDir, o.worldNormal);

                TRANSFER_SHADOW(o);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
                fixed3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;

                float halfLambert = dot(worldLightDir, worldNormal) * 0.5 + 0.5;
                float3 diffuse = _LightColor0.rgb * _Color.rgb * halfLambert;
                // 进行CubeMap纹理采样
                fixed3 reflection = texCUBE(_CubeMap, i.worldRefl).rgb * _ReflectColor.rgb;
                // 求阴影和光照衰减
                UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
                // 
                fixed3 color = ambient + lerp(diffuse, reflection, _ReflectAmount) * atten;

                return fixed4(color, 1.0);
            }

            ENDCG
        }
    }
}
