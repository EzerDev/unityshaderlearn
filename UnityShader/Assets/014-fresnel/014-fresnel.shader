﻿Shader "Unlit/014-fresnel"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _FresnelScale ("Fresnel Scale", Range(0, 1)) = 0.5
        _CubeMap ("Reflection CubeMap", Cube) = "_Skybox" {}
    }

    SubShader
    {
        Tags{"LightMode" = "ForwardBase"}
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "AutoLight.cginc"

            fixed4 _Color;
            float _FresnelScale;
            samplerCUBE _CubeMap;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
                float3 worldViewDir : TEXCOORD1;
                float3 worldRefl : TEXCOORD2;
                float3 worldPos : TEXCOORD3;
                SHADOW_COORDS(2)
            };

            v2f vert(a2v i)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(i.vertex);
                o.worldPos = mul(unity_ObjectToWorld, i.vertex);
                o.worldNormal = UnityObjectToWorldNormal(i.normal);
                o.worldViewDir = UnityWorldSpaceViewDir(o.worldPos);
                // 通过反向视角方向 和法线向量 求出 反射方向
                o.worldRefl = reflect(-o.worldViewDir, o.worldNormal);
                TRANSFER_SHADOW(o);
                return o;
            }

            fixed4 frag(v2f i) : SV_TARGET
            {
                fixed3 worldNormal = normalize(i.worldNormal);
                fixed3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
                fixed3 worldViewDir = normalize(i.worldViewDir);

                fixed3 ambinet = UNITY_LIGHTMODEL_AMBIENT.rgb;
                                                              
                float halfLambert = dot(worldLightDir, worldNormal) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * _Color.rgb * halfLambert;

                UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);
                // 求出
                fixed3 reflection = texCUBE(_CubeMap, i.worldRefl).rgb;
                // 求出Fresnel 变量
                fixed fresnel = _FresnelScale + (1 - _FresnelScale) * pow(1 - dot(worldViewDir, worldNormal), 5);

                fixed3 color = ambinet + lerp(diffuse, reflection, saturate(fresnel)) * atten;

                return fixed4(color, 1.0);
            }

            ENDCG
        }
    }
}
