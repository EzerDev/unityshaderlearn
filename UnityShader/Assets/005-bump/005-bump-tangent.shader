﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/005-bump"
{
    Properties
    {
        _MainTex("MainTex", 2D) = "white" {}
        _Color("Color", Color) = (1,1,1,1)

        _Specular("Specular", Color) = (1,1,1,1)
        _Gloss("Gloss", Range(8, 256)) = 8

        _BumpTex("BumpTex", 2D) = "white" {}
        _BumpScale("BumpScale", float) = 1.0
    }

    SubShader
    {
        Tags{"LightMode"="ForwardBase"}
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            fixed4 _Specular;
            float _Gloss;
            
            sampler2D _BumpTex;
            float4 _BumpTex_ST;
            float _BumpScale;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 lightDir : TEXCOORD0;
                float3 viewDir : TEXCOORD1;
                float4 uv : TEXCOORD2;
            };

            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);

                // 副切线
                float3 bitangent = cross(v.normal, v.tangent.xyz) * v.tangent.w;
                // 组成切线空间矩阵
                float3x3 rotation = float3x3(v.tangent.xyz, bitangent, v.normal);

                o.lightDir = mul(rotation, ObjSpaceLightDir(v.vertex)).xyz;
                o.viewDir = mul(rotation, ObjSpaceViewDir(v.vertex)).xyz;

                o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.texcoord, _BumpTex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 tangentLightDir = normalize(i.lightDir);
                float3 tangentViewDir = normalize(i.viewDir);

                fixed4 packedNormal = tex2D(_BumpTex, i.uv.zw);
                fixed3 tangentNormal = UnpackNormal(packedNormal);
                tangentNormal.xy *= _BumpScale;
                //tangentNormal.z = sqrt(1.0 - saturate(dot(tangentNormal.xy, tangentNormal.xy)));

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb ;

                fixed3 albedo = tex2D(_MainTex, i.uv.xy) * _Color.rgb;

                // 漫反射
                float halfLambert = dot(tangentNormal, tangentLightDir) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * albedo * halfLambert;

                // 高光反射
                float3 halfDir = normalize(tangentLightDir + tangentViewDir);
                fixed3 specular = _LightColor0.rgb * _Specular * pow(saturate(dot(tangentNormal, halfDir)), _Gloss);

                fixed3 color = ambient + diffuse + specular;
                return fixed4(color, 1.0);

            }
            ENDCG
        }
    }
}
