﻿Shader "Unlit/005-bump-tangent-test"
{
   Properties
   {
       _MainTex("MainTex", 2D) = "white" {}
       _Color("Color", Color) = (1,1,1,1)

       _BumpTex("BumpTex", 2D) = "white" {}
       _BumpScale("BumpScale", float) = 1.0

       _Specular("Specular", Color) = (1,1,1,1)
       _Gloss("Gloss", Range(8, 256)) = 8.0
   }

   SubShader
   {
       Tags{"LightMode"="ForwardBase"}
       Pass
       {
           CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            sampler2D _BumpTex;
            float4 _BumpTex_ST;
            float _BumpScale;
            fixed4 _Specular;
            float _Gloss;

            struct a2v
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 uv : TEXCOORD0;
                float3 tangentViewDir : TEXCOORD1;
                float3 tangentLightDir : TEXCOORD2;
            };

            v2f vert(a2v v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);

                float3 binormal = cross(v.tangent.xyz, v.normal) * v.tangent.w;
                float3x3 rotation = float3x3(v.tangent.xyz, binormal, v.normal);

                o.tangentViewDir = mul(rotation, ObjSpaceViewDir(v.vertex));
                o.tangentLightDir = mul(rotation, ObjSpaceLightDir(v.vertex));

                o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.uv.zw = TRANSFORM_TEX(v.texcoord, _BumpTex);

                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float3 viewDir = normalize(i.tangentViewDir);
                float3 lightDir = normalize(i.tangentLightDir);

                fixed4 packedNormal = tex2D(_BumpTex, i.uv.zw);
                fixed3 normal = UnpackNormal(packedNormal);
                normal *= _BumpScale;

                // 环境光
                fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;

                //漫反射
                fixed3 albedo = tex2D(_MainTex, i.uv.xy) * _Color.rgb;
                float3 halfLambert = dot(lightDir, normal) * 0.5 + 0.5;
                fixed3 diffuse = _LightColor0.rgb * albedo * halfLambert;

                // 高光反射
                float3 halfDir = normalize(lightDir + viewDir);
                fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(saturate(dot(normal, halfDir)), _Gloss);

                fixed3 color = ambient + diffuse + specular;
                return fixed4(color, 1.0);
            }

           ENDCG
       }
   }
}
