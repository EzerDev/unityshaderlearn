﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/002-diffuse"
{
    
   Properties
   {
       _Diffuse("Diffuse", Color) = (1,1,1,1)
   }

   SubShader
   {
       Tags{"LightMode"="ForwardBase"}
       
       Pass
       {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"
                #include "Lighting.cginc"

                fixed4 _Diffuse;

                struct a2v
                {
                    float4 vertex : POSITION;
                    float3 normal : NORMAL;
                };

                struct v2f
                {
                    float4 pos : SV_POSITION;
                    fixed3 worldNormal : TEXCOORD0;
                };

                v2f vert(a2v v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);

                    o.worldNormal = normalize(mul(v.normal, unity_ObjectToWorld));

                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb;

                    float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
                    float halfLambert = dot(lightDir, i.worldNormal) * 0.5 + 0.5;
                    fixed3 diffuse = _LightColor0.rgb * _Diffuse * halfLambert;

                    fixed4 color = fixed4( ambient + diffuse, 1.0);
                    return color;
                }
            ENDCG
       }
   }
}
